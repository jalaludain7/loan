from flectra import models,fields,_,api
from datetime import datetime
from dateutil.rrule import rrule, MONTHLY
import calendar
from flectra.exceptions import ValidationError





class LoanRequest(models.Model):
    _name="loan.request"

    name=fields.Many2one('res.partner',string="Name",required=True)
    requested_amount=fields.Float(string="Requested Amount",required=True)
    

    start_date=fields.Date(string="Start Date",required=True,default=datetime.now())
    end_date=fields.Date(string="End Date")
    minimum_amount=fields.Float(string="Minmum Amount")
    payment_method=fields.Selection([('auto','auto'),('manual','manual')],string="Payment Method",default="auto",required=True)
    enter_amount=fields.Float(string="Enter Amount")
    request_line_ids=fields.One2many('loan.request.line','loan_request_id',string="Request Line")
    state=fields.Selection([('draft','Draft'),('validate','Validate'),('approve','Approve'),('cancel','Cancel')],default="draft",string="State",readonly=True)
    loan_history_ids=fields.One2many('loan.history','loan_request_id')
    forward_to=fields.Many2one('res.users',string="Forward To")
    



    def email_forward_to(self):
        print("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPP :  ",self.forward_to.login)
        template=self.env.ref('loan_request.forward_email')
        
        template.send_mail(self.id,force_send=True)



    
   
    @api.onchange('payment_method')
    def onchange_payment_method(self):
        if self.payment_method=='auto':
            self.enter_amount=0

    
    @api.onchange('enter_amount')
    def onchange_enter_amount(self):
        for o in self:
            st_date=o.start_date.replace(day=1)
            if o.end_date:
                end_date=o.end_date.replace(day=1)
                mon_list = [dt for dt in rrule(MONTHLY, dtstart=st_date, until=end_date)]
                month_count=len(mon_list)-1
                check_amount=o.enter_amount*month_count
                if check_amount<o.requested_amount:
                    raise ValidationError("Enter amount is too small")
                elif o.enter_amount>o.requested_amount:
                    raise ValidationError("Enter amount is increase from requested amount")




    
    @api.onchange('requested_amount','end_date')
    def onchange_requested_amount(self):
        if self.end_date and self.requested_amount:
            for o in self:
                # delta=relativedelta.relativedelta(o.end_date, o.start_date)
                # months=int(delta.months)
                st_date=self.start_date.replace(day=1)
                end_date=self.end_date.replace(day=1)
                mon_list = [dt for dt in rrule(MONTHLY, dtstart=st_date, until=end_date)]
                month_count=len(mon_list)-1
                
                if(month_count>0):
                    o.minimum_amount=o.requested_amount/month_count

                else:
                    o.minimum_amount=0

    def getData(self):
        return "jalal"

    def calculate_loan(self):
        self.request_line_ids.unlink()
        
        
        for o in self:
           
            st_date=o.start_date.replace(day=1)
            end_date=o.end_date.replace(day=1)

            mon_list = [dt for dt in rrule(MONTHLY, dtstart=st_date, until=end_date)]

          
            
            m_name=[]
            st_month=[]
            end_month=[]
            for i in range(len(mon_list)):
                if i>0:
                    m_no=mon_list[i].month
                    name=calendar.month_abbr[m_no]
                    m_name.append(name)
                    st_month.append(name+"-1")
                    res = calendar.monthrange(mon_list[i].year, mon_list[i].month)
                    end_month.append(name+"-"+str(res[1]))
            ################################

            #Amount Accessing
            pay_amount=[0]*len(mon_list)
           
            m_amount=0


            if(self.enter_amount==0):
                for i in range(len(mon_list)):
                    pay_amount[i]=(self.minimum_amount)
                
            else:
                for i in range(len(mon_list)):
                    m_amount+=self.enter_amount
    
                    if(m_amount>self.requested_amount):
                        print("amount increase")
                        m_amount-=self.enter_amount
                        remain_amount=self.requested_amount-m_amount
                       
                        pay_amount[i]=remain_amount
                        break
        
                    else:
        
                        pay_amount[i]=self.enter_amount
                   
                




            ###############################
            
            for i in range(len(m_name)):
                
                values={
                    'months': m_name[i],
                    'Amount':pay_amount[i],
                    'start_date':st_month[i],
                    'end_date':end_month[i],
                    'loan_request_id':self.id
    


                }
                
                self.env['loan.request.line'].create(values)

    @api.model
    def create(self,vals):

        res=super(LoanRequest,self).create(vals)

        # res.loan_history_ids.create({
        #     'name':'created by',
        #     'user_id':self.env.user.id,
        #     'date_time':datetime.today(),
        #     'loan_request_id':self.id
        #     })

        res.loan_history_ids=[(0,0,{
            'name':'created by',
            'user_id':self.env.user.id,
            'date_time':datetime.today(),
            'loan_request_id':self.id
            })]
        
      
        self.send_mail_request(res)

        return res
    

    def send_mail_request(self,record):
        template=self.env.ref('loan_request.loan_request_email')
        
        template.send_mail(record.id,force_send=True)

    
    def loanDraft(self):
        self.loan_history_ids.create({
            'name':'draft by',
            'user_id':self.env.user.id,
            'date_time':datetime.today(),
            'loan_request_id':self.id
            })

        self.state="draft"

    def loanValidate(self):
        self.loan_history_ids.create({
            'name':'Validate by',
            'user_id':self.env.user.id,
            'date_time':datetime.today(),
            'loan_request_id':self.id
            })

        self.state="validate"

    def loanApprove(self):
        self.loan_history_ids.create({
            'name':'Approve by',
            'user_id':self.env.user.id,
            'date_time':datetime.today(),
            'loan_request_id':self.id
            })

        self.state="approve"

    def loanCancel(self):
        self.loan_history_ids.create({
            'name':'Cancel by',
            'user_id':self.env.user.id,
            'date_time':datetime.today(),
            'loan_request_id':self.id
            })

        self.state="cancel"

    









    
    
            

        

