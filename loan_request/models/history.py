from flectra import fields,_,models

class HistoryLoan(models.Model):
    _name="loan.history"

    name=fields.Char()
    user_id=fields.Many2one('res.users')
    date_time=fields.Date()
    loan_request_id=fields.Many2one('loan.request')
