from flectra import models,fields,_


class LineRequest(models.Model):
    _name="loan.request.line"

    months=fields.Char()
    start_date=fields.Char()
    end_date=fields.Char()
    Amount=fields.Float()
    state=fields.Selection([('unPaid','unPaid'),('paid','paid')],default="unPaid")
    loan_request_id=fields.Many2one('loan.request',string="Loan Lines")


    