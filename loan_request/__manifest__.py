{
    'name': "Loan Request",
    'summary': "Manage loan request",
    'description': """ loan request""",
    'author': "Abdul Jalal",
    'website': "http://www.google.com",
    'category': 'Uncategorized',
    'version': '2.0.1',
    'depends': ['base','mail'],
    'sequence':-120,
    'data' : [
            'security/security.xml',
            'security/ir.model.access.csv',
            'data/forward_email.xml',
            'data/loan_request_email.xml',
            'views/loan_request_view.xml'
            ]

}